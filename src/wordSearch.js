var fs = require("fs");
// Author: Donald Bowler

// depth first search algorithm
function findIfExists(rowIndex, colIndex, i, word, matrix, R, C, positions, visited) {
    // Base case: if this is the last character, then return true
    if (i == word.length) {
        return true;
    }
    
    // Return false for all invalid cases
    if (
        rowIndex < 0 ||
        rowIndex >= R ||
        colIndex < 0 ||
        colIndex >= C ||
        word[i] !== matrix[rowIndex][colIndex] 
    ) {
        return false;
    } 
    
    const moves = [
        [1, 1],       // Down-Right
        [-1, 0],      // Up
        [1, 0],       // Down
        [0, -1],      // Left
        [0, 1],       // Right
        [-1, -1],     // Up-Left
        [-1, 1],      // Up-Right
        [1, -1],      // Down-Left
    ];

    // Recursively check in all eight directions
    for (const [dr, dc] of moves) {
        if (findIfExists(rowIndex + dr, colIndex + dc, i+1, word, matrix, R, C, positions, visited)) {
            // If a match is found, push the current position to the positions array
            if (matrix[rowIndex][colIndex] === word[0] || matrix[rowIndex][colIndex] === word[word.length - 1]) 
            {
                positions.push([rowIndex, colIndex]);
            }
            return true;

        }
    }
    

    return false;
}

fs.readFile("input.txt", "utf8", function(err, data) {
    if (err) {
        return console.log(err);
    }

    var lines = data.split("\n");
    var gridSizeLine = lines[0];
    var gridSizeParts = gridSizeLine.split("x");
    var numRows = parseInt(gridSizeParts[0]);
    var numCols = parseInt(gridSizeParts[1]);
    var matrix = [];
    var wordArray = [];
    var results = [];

    // Collect the word search characters, starting from the second line and put them into a matrix
    for (let i = 1; i <= numRows; i++) {
        //correctly parse data to leave no carriage return characters
        const line = lines[i].replace(/[\r\n]+/gm, "");
        var elements = line.split(" ");
        matrix.push(elements);
    }

    // Extract words from the wordArray
    for (let i = numRows + 1; i < lines.length; i++) {
        const trimmedWord = lines[i].trim();
        if (trimmedWord) {
            wordArray.push(trimmedWord);
        }
    }

    //match wordString to matrix
    for (let wordString of wordArray) {
        for (let h = 0; h < matrix.length; h++) {
            for (let k = 0; k < matrix[0].length; k++) {
                if (matrix[h][k] === wordString[0]) {
                    const positions = [];
                    const visited = new Set();
                    if (findIfExists(h, k, 0, wordString, matrix, numRows, numCols, positions, visited)) {
                        positions.reverse();
                            results.push({ word: wordString, positions: [...positions] });
                    }
                }
            }
        }
    }

    // print out results into console
    for (let result of results) {
        const wordString = result.word;
        let positionsString = "";
        for (let position of result.positions) {
            positionsString += `${position[0]}:${position[1]} `;
        }
        console.log(`${wordString} ${positionsString}`);
    }
    
});

